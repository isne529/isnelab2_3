#include<iostream>
#include<ctime>
#include<algorithm>
#include<vector>
#include<list>
using namespace std;

int main() {
	int MaxCustomer, MinCustomer, MaxServiceTime, MinServiceTime;
	int Customer, NumCashier;
	double WaitAVG = 0;
	vector<int> ArriveTime;
	vector<int> ServiceTime;
	vector<int> LeftTime;
	vector<int> WaitTime;
	list<int> Casher;

	cout << "Max Customers : ";
	cin >> MaxCustomer;
	cout << "Min Customers : ";
	cin >> MinCustomer;
	cout << "Max service time : ";
	cin >> MaxServiceTime;
	cout << "Min service time : ";
	cin >> MinServiceTime;
	cout << "Number of casher : ";
	cin >> NumCashier;

	srand(time(0));
	Customer = rand() % (MaxCustomer - MinCustomer) + MinCustomer;		//random number of customers
	cout << "Number of Customer : " << Customer << endl;

	for (int i = 0; i < Customer; i++) {
		ArriveTime.push_back(rand() % 240);		//random customer arrive time 
	}
	for (int i = 0; i < Customer; i++) {
		ServiceTime.push_back(rand() % (MaxCustomer - MinCustomer) + MinCustomer);		//random srevice time of customer
	}
	sort(ArriveTime.begin(), ArriveTime.end());		//sort random number from 0-240
	for (int i = 0; i < Customer; i++) {
		while (i<NumCashier) {		//number of customer lower than number of cashier ,they don't need to wait 
			WaitTime.push_back(0);		//store wait time of customer ,the customer don't need to wait so wait time = 0;	
			LeftTime.push_back(ArriveTime.at(i) + ServiceTime.at(i));		//store left time of customer
			Casher.push_back(LeftTime.at(i));		//store timer for cashier
			Casher.sort();		//sort cashier timer from the lowest
			i++;
		}
		if (ArriveTime.at(i) > Casher.front()) {		//if customer arrive when cashiers are free 
			WaitTime.push_back(0);	//store wait time of customer ,the customer don't need to wait so wait time = 0;	
		}
		else {		//if customer arrive when cashiers aren't free 
			WaitTime.push_back(Casher.front() - ArriveTime.at(i));		//store wait time of customer ,the customer need to wait untill the cashiers are free  			
			WaitAVG += WaitTime.at(i);		//
		}
		LeftTime.push_back(ArriveTime.at(i) + ServiceTime.at(i) + WaitTime.at(i));
		Casher.push_back(LeftTime.at(i));
		Casher.pop_front();		//remove the lowest timer of cashier
		Casher.sort();
	}

	for (int i = 0; i < Customer; i++) {
		cout << "Customer " << i + 1 << " :: Arrive Time " << ArriveTime.at(i) << " :: Waiting Time " << WaitTime.at(i) << " :: Left Time " << LeftTime.at(i) << endl;
	}
	cout << "Wait time average : " << WaitAVG / Customer << endl;

	system("pause");
}